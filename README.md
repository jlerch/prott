# PROTT - Live Translation of Verbatim Records 
## Disclaimer
The idea of this software and parts of algorithmical concept originated with Github user theAn0va, who developed [his own variant](https://github.com/theAn0va/LTP) for the BD student club in Ilmenau, Germany.

## Description
Sometimes you participate in meetings where a protocol is written live during the conference e.g. with HedgeDoc or EtherPad. In this case it could be possible that you want to have the protocol translated live, e.g. if there are participants that would like to read the protocol in another language than the one in which the protocol is written.

This software reads the original protocol-pad and posts a translated version repeatedly into another pad.

The translation is unidirectional. Words written in the translated version are not translated nor written into the original protocol-pad.

## Installation and Usage in Developement Mode
1. Get the project:
```
$ git clone https://gitlab.fem-net.de/technik/protocol-translation-live.git
$ cd protocol-translation-live
$ cp example.conf config.conf
```

2. Edit `config.conf` to fit your needs.

> Currently it is only possible for the destination to be an etherpad instance. The source could be either a hedgedoc or an etherpad instance.

> The pads themselves have to exist before you start the package

3. Create a file named `api_keys.conf` in the project's root directory, using the template below and enter the API-Keys of the services you want to use.
```
[default]
etherpad_key = <etherpad-key>
libretranslate_key = <libretranslate-key>
```

4. Create a venv to install the package in:
```
$ python3 -m venv venv
$ source venv/bin/activate
```

4. install the package by typing
```
$ pip install --editable .
```

5. Now you can execute the package by typing
```
$ python3 prott
```