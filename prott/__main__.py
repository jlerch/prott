import atexit
from configparser import ConfigParser, Error
from difflib import SequenceMatcher
from itertools import takewhile
from pathlib import Path
from typing import List, Tuple

from prott import utils, log
from prott.prott import Prott
from prott.pad import Etherpad, HedgedocNote, Pad
from prott.translation import DeepL, LibreTranslate, TranslationService


def main() -> None:
    try:
        utils.validate_config(config_path_string=config_path_string)
    except (FileNotFoundError, OSError, ValueError) as e:
        utils.exit_with_error(e)

    api_keys: ConfigParser = ConfigParser()
    config: ConfigParser = ConfigParser()

    config.read(Path(config_path_string))
    api_keys.read(Path(config["default"]["api_key_path"]))

    # create src pad
    src_pad: Pad
    if config["source"]["kind"] == "hedgedoc":
        src_pad = HedgedocNote(config["source"]["url"])
    elif config["source"]["kind"] == "etherpad":
        src_pad = Etherpad(config["source"]["url"], api_keys["default"]["etherpad_key"])

    # create dst pad
    dst_pad: Pad
    if config["destination"]["kind"] == "etherpad":
        dst_pad = Etherpad(
            config["destination"]["url"], api_keys["default"]["etherpad_key"]
        )
    elif config["destination"]["kind"] == "hedgedoc":
        dst_pad = HedgedocNote(config["destination"]["url"])

    # create translation
    src_lang: str = config["source"]["language"]
    dst_lang: str = config["destination"]["language"]

    translator: TranslationService
    if config["translation"]["service"] == "libretranslate":
        translator = LibreTranslate(
            config["translation"]["service_url"],
            api_keys["default"]["libretranslate_key"],
        )
    elif config["translation"]["service"] == "deepl":
        translator = DeepL(api_keys["default"]["deepl_key"])

    prott: Prott = Prott(
        src_pad=src_pad,
        dst_pad=dst_pad,
        src_lang=src_lang,
        dst_lang=dst_lang,
        t=translator,
        header_size=config.getint("default", "header_size"),
    )

    while True:
        prott.pull_diff_translate_push()


if __name__ == "__main__":
    main()
