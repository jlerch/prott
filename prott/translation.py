from abc import ABC, abstractmethod
from typing import List

import deepl
import requests


class TranslationService(ABC):
    @abstractmethod
    def translate(self, src_string: str, src_lang: str, dst_lang: str) -> str:
        pass

    @abstractmethod
    def get_supported_src_languages(self) -> List[str]:
        pass

    @abstractmethod
    def get_supported_dst_languages(self) -> List[str]:
        pass


class DeepL(TranslationService):
    def __init__(self, api_key: str):
        self.api_key = api_key

        self.deepl = deepl.Translator(api_key)

        # Check for Usage
        usage = self.deepl.get_usage()
        assert not usage.any_limit_reached

    def translate(self, src_string: str, src_lang: str, dst_lang: str) -> str:
        # Change non-specific english to british english
        if src_lang.upper() == "EN":
            src_lang = "EN-GB"
        if dst_lang.upper() == "EN":
            dst_lang = "EN-GB"

        # Check for supported language or detect src language
        assert (
            src_lang.upper() in [x.code for x in self.deepl.get_source_languages()]
            or src_lang == ""
        )
        assert dst_lang.upper() in [x.code for x in self.deepl.get_target_languages()]
        if not src_string:
            return ""

        return str(
            self.deepl.translate_text(
                src_string, source_lang=src_lang, target_lang=dst_lang
            ).text
        )

    def get_supported_src_languages(self) -> list[str]:
        return [x.code for x in self.deepl.get_source_languages()]

    def get_supported_dst_languages(self) -> list[str]:
        return [x.code for x in self.deepl.get_target_languages()]


class LibreTranslate(TranslationService):
    def __init__(self, url: str, api_key: str):
        self.url = url
        self.api_key = api_key

    def translate(self, src_string: str, src_lang: str, dst_lang: str) -> str:
        r = requests.post(
            self.url + "/translate",
            data={
                "q": src_string if src_string != "" else " ",
                "source": src_lang,
                "target": dst_lang,
                "format": "text",
            },
        )
        print(r.json())
        return str(r.json()["translatedText"])

    def get_supported_src_languages(self) -> List[str]:
        r = requests.get(self.url + "/languages")
        return list(str(val["code"] for val in r.json()))

    def get_supported_dst_languages(self) -> List[str]:
        return self.get_supported_src_languages()
