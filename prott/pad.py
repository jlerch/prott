from abc import ABC, abstractmethod
from urllib.parse import quote
from prott import utils
import requests


class Pad(ABC):
    @abstractmethod
    def read(self) -> str:
        pass

    @abstractmethod
    def write(self, string: str) -> None:
        pass

    @abstractmethod
    def append(self, string: str) -> None:
        pass


class Etherpad(Pad):
    """
    For reading and writing the official HTTP API of Etherpad was used

    The URL contains a substring `/url/$NUMBER`. Where $NUMBER is the version
    of the API that is used for the respective function.

    """

    def __init__(self, url: str, api_key: str):
        url_parted = url.split("/p/")
        self.main_url, self.pad_id = url_parted
        self.api_key = api_key

    def read(self) -> str:
        r = requests.get(
            self.main_url
            + "/api/1/getText?apikey="
            + self.api_key
            + "&padID="
            + self.pad_id
        )

        r_json = r.json()
        if not r_json["code"] == 0:
            utils.exit_with_error(r_json["message"])

        return str(r_json["data"]["text"])

    def write(self, string: str) -> None:
        r = requests.post(
            url=self.main_url
            + "/api/1/setText?apikey="
            + self.api_key
            + "&padID="
            + self.pad_id,
            data={"text": string},
        )

        r_json = r.json()
        if not r_json["code"] == 0:
            print("read failed")
            utils.exit_with_error(r_json["message"])

    def append(self, string: str) -> None:
        string = quote(string)
        r = requests.get(
            self.main_url
            + "/api/1.2.13/appendText?apikey="
            + self.api_key
            + "&padID="
            + self.pad_id
            + "&text="
            + string
        )

        r_json = r.json()
        if not r_json["code"] == 0:
            utils.exit_with_error(r_json["message"])


class HedgedocNote(Pad):
    def __init__(self, url: str):
        self.url = url

    def read(self) -> str:
        r = requests.get(self.url + "/download")
        return r.text

    def write(self, string: str) -> None:
        raise NotImplementedError()

    def append(self, string: str) -> None:
        raise NotImplementedError()
