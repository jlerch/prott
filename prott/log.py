from logging import getLogger
from rich.logging import RichHandler

logger = getLogger("protocol_translation_live")
logger.addHandler(RichHandler())
