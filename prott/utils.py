import os
import sys
from configparser import ConfigParser
from prott.log import logger
from urllib.parse import urlparse
from pathlib import Path
from typing import Any


def exit_with_error(err_msg: Any) -> None:
    logger.error(err_msg)
    sys.exit(1)


def is_url_valid(url: str) -> bool:
    result = urlparse(url)
    return all([result.scheme, result.netloc])


def validate_config(config_path_string: str) -> None:
    """Validates a config file.

    Raises errors: FileNotFoundError, OSError, ValueError
    """
    config_path: Path = Path(config_path_string)

    if not config_path.is_file():
        raise FileNotFoundError("Config file was not found.")

    config = ConfigParser()

    if not config.read(config_path):
        raise OSError("Given file could not be read.")

    if config.getint("main", "header_size") < 0:
        raise ValueError("'header_size' in [main] is < 0. It should be >= 0.")

    if not Path(config["main"]["api_key_path"]).is_file():
        raise FileNotFoundError("File at path .conf does not exist.")

    if config["translation"]["service"] not in ["libretranslate", "deepl"]:
        raise ValueError("Given translation service is not valid.")

    if config["translation"]["service"] != "deepl" and not is_url_valid(
        config["translation"]["service_url"]
    ):
        raise ValueError("Translator URL is not valid")

    for x in ["source", "destination"]:
        if config[x]["kind"] not in ["etherpad", "hedgedoc"]:
            raise ValueError("Given pad service is not valid.")

        if not is_url_valid(config[x]["url"]):
            raise ValueError("Given pad service URL is not valid.")

        # TODO: fix language check
        if len(config[x]["language"]) != 2:
            raise ValueError(f"{x} Language")

        # # ..check if src_lang and dst_lang are supported
        # supported_languages = t.get_supported_languages()
        # if src_lang not in supported_languages:
        #     utils.exit_with_error(
        #         "config.conf: 'language = "
        #         + config["destination"]["kind"]
        #         + "' in [source] is not valid. Supported languages are: "
        #         + ", ".join(supported_languages)
        #     )

        # if dst_lang not in supported_languages:
        #     utils.exit_with_error(
        #         "config.conf: 'language = "
        #         + config["destination"]["kind"]
        #         + "' in [source] is not valid. Supported languages are: "
        #         + ", ".join(supported_languages)
        #     )
