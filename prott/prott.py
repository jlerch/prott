from typing import List, Tuple, Generator
from .pad import Pad
from .translation import TranslationService
from itertools import takewhile
from difflib import SequenceMatcher


class Prott:
    def __init__(
            self,
            src_pad: Pad,
            dst_pad: Pad,
            src_lang: str,
            dst_lang: str,
            t: TranslationService,
            header_size: int,
    ):
        self._src_pad: Pad = src_pad
        self._dst_pad: Pad = dst_pad
        self._src_lang: str = src_lang
        self._dst_lang: str = dst_lang
        self._t: TranslationService = t
        self._old_body: List[str] = []
        self._header_size: int = header_size

        # initial translation
        self._dst_pad.write("initializing...")

        initial_text: List[str] = src_pad.read().splitlines()
        header: List[str] = initial_text[0:header_size]
        self._body: List[str] = initial_text[header_size:]
        self._translated_body: List[str] = self._translate_with_leading_whitespace(
            self._body
        )

        self._dst_pad.write("\n".join(header + self._translated_body))

        # ..prepare further translation
        self._s: SequenceMatcher[str] = SequenceMatcher()
        self._joined_header: str = "\n".join(header) + "\n" if len(header) > 0 else ""

    def pull_diff_translate_push(self) -> None:
        # it is assumed, that the translation service definitely translates the protocol line by line

        # [old_]body and [old_]translated_body are lists of lines
        self._old_body = self._body

        # read body (without header)
        self._body = self._src_pad.read().splitlines()[self._header_size:]

        # sequence matcher get_opcodes
        self._s.set_seqs(self._old_body, self._body)
        opcodes: List[Tuple[str, int, int, int, int]] = self._s.get_opcodes()

        # translate opcodes and insert the translated text into the old_translated_body, thus updating translated_body
        num_added_lines: int = 0
        val: Tuple[str, int, int, int, int, List[str]]
        for val in self._translate_opcodes(opcodes):
            if val[0] == "delete":
                del self._translated_body[
                    val[1] + num_added_lines: val[2] + num_added_lines
                    ]
                num_added_lines -= val[2] - val[1]

            elif val[0] == "replace":
                self._translated_body[
                val[1] + num_added_lines: val[2] + num_added_lines
                ] = val[5]

            elif val[0] == "insert":
                self._translated_body[
                val[1] + num_added_lines: val[2] + num_added_lines
                ] = val[5]
                num_added_lines += val[4] - val[3]

            else:
                assert False, f"The value '{val[0]}' should not be possible"

        # write
        self._dst_pad.write(self._joined_header + "\n".join(self._translated_body))

    def _translate_with_leading_whitespace(self, lines: List[str]) -> List[str]:
        """translate list of strings with trailing whitespace lines:
        * strip leading lines consisting of whitespace,
        * strip leading space string from each line
        * add both when writing to dst_pad
        This has to be done, because libretranslate ignores leading whitespace,
        leading to incorrect order of the translated lines.
        """
        # filter leading space lines
        space_lines = list(takewhile(lambda line: line.isspace() or not line, lines))

        # filter leading space strings of each line
        lstrings: List[str] = []
        lines_to_translate: List[str] = []
        for line in lines[len(space_lines):]:
            # compute leading strings
            lchars = list(takewhile(lambda character: character.isspace(), line))
            lstring = "".join(lchars)
            lstrings.append(lstring)
            # create lines where leading space strings are stripped
            lines_to_translate.append(line[len(lchars):])

        # translate it
        text_to_translate = "\n".join(lines_to_translate)

        translated_lines = self._t.translate(
            text_to_translate, self._src_lang, self._dst_lang
        ).splitlines()

        # append leading strings per line
        translated_lines_lspace: List[str] = []
        for space, line in zip(lstrings, translated_lines):
            translated_lines_lspace.append(space + line)

        return space_lines + translated_lines_lspace

    def _translate_single_opcode(
            self, opcode: Tuple[str, int, int, int, int]
    ) -> List[str]:
        """
        compute the text in the lines that have been changed and translate it,
        except for the case, that the tag is 'delete' which is trivial
        """
        if opcode[0] == "delete":
            return []
        else:
            lines = self._body[opcode[3]: opcode[4]]

            return self._translate_with_leading_whitespace(lines)

    def _translate_opcodes(
            self, opcodes: List[Tuple[str, int, int, int, int]]
    ) -> Generator[Tuple[str, int, int, int, int, List[str]], None, None]:
        # val: Tuple[str, int, int, int, int]
        for val in opcodes:
            # ..add all elements of 'opcodes' to it except those with the 'equal'-tag
            if val[0] != "equal":
                e0, e1, e2, e3, e4 = val
                # ..apply translate_opcodes
                yield (e0, e1, e2, e3, e4, self._translate_single_opcode(val))
