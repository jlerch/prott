from configparser import ConfigParser
import json
import requests
from functools import cache
import protocol_translation.translation as translation


def init():
    api_keys = ConfigParser()
    api_keys.read("api_keys.conf")

    t = LibreTranslate("https://translate.fem-net.de", api_keys["libretranslate_key"])

    return t


def test_translate():
    t = init()
    text_de = "Dies ist ein Ball."
    text_en = "this is a ball."
    assert t.translate(text_de, "de", "en") == text_en


def test_translate_empty():
    t = init()
    text_de = ""
    text_en = ""
    assert t.translate(text_de, "de", "en") == text_en
