from configparser import ConfigParser
from functools import cache
from protocol_translation.pad import Etherpad

test_string_1 = "123\n123\n123\n123\n"
test_string_2 = "124\n124\n124\n124\n"


@cache
def init():
    global test_string_1
    global test_string_2

    api_keys = ConfigParser()

    api_keys.read("api_keys.conf")

    e = Etherpad(
        "https://collab.fem-net.de/pad/p/wOdasS2kvJwC9BDb",
        api_keys["default"]["etherpad_key"],
    )

    return e


def test_init():
    e = init()
    assert isinstance(e, Etherpad)


def test_read():
    e = init()
    assert e.read() == test_string_1


def test_write():
    e = init()
    e.write(test_string_2)
    assert e.read() == test_string_2
    e.write(test_string_1)
