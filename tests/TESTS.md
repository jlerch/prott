# Tests

## unit tests
See the files named `test_<name>.py` in this directory

## Other tests

### Experiment 1: Etherpad-API: Lorem Ipsum paragraphs
How does the system behave if a big protocol is read, and written into another pad at once without translation or any other computing steps?

Lorem Ipsum generator: https://loremipsum.io/generator/?n=1&t=p

At every step a specific number of paragraphs were created, copied and pasted into the source pad

|number of paragraphs|time to paste| 
|--|--|
|1|ca. 1s|
|2|ca. 1s|
|4|ca. 1s|
|8|ca. 1s|
|13|ca. 1s|
|14|no response for at least 10s|

So somehow there is an upper bound of how much text that can be pastet at once into the destination pad.

### Experiment 2: Etherpad-API: Lorem Ipsum paragraphs II
Same situation as in Exp. 1.

13 paragraphs of Lorem Ipsum where pastet into the source pad.

After that one paragraph of Lorem Ipsum was added below in the source pad. The destination pad added this new paragraph within 1s.

Then, 4 more Lorem Ipsum paragraphs were added step by step. Same outcome.

Then, 13 more Lorem Ipsum paragraphs were added at once. Same outcome.

### Experiment 3: Etherpad-API: Lorem Ipsum paragraphs III
Same situation as in Exp. 1.

30 paragraphs of Lorem Ipsum are in both source and destination pad (see Exp. 2). After that the second paragraph was edited.

The destination pad updated within 1s.

### Experiment 4: Etherpad-API: Determine upper bound data to write at once
Same situation as in Exp. 1.

Determine a more exact size than "13 paragraphs of Lorem Ipsum" by using bisection Method and a random text generator: https://slothman.dev/text-generator/

The upper bound is 9783 Byte.

### Experiment 5: Etherpad-API: Determine true reason of upper bound
Same situation as in Exp. 1.

The same pad was opened with two laptops and the translation-software turned off.

Then, experiment 1 was repeated. The results were the same.

After pasting an amount of text of around 10000 Bytes (greater than 9783 Bytes) into the source pad the translation software was turned on. it pasted the text instantly to the destination pad

Hence the true reason of the upper bound is the bottleneck between the browser and the etherpad-service.

### Conclusion of Experiments 1, 2, 3, and 4
The transfer of text from one etherpad to another works flawlessly.