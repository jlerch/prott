import configparser
import os
import sys
from configparser import ConfigParser
from pathlib import Path
import webbrowser
from urllib.parse import urlparse
import prott.utils
from prott import prott as prott_main
from prott import translation, utils, pad
from PySide6.QtCore import QFile, QTextStream, QThread, QObject, Signal
from PySide6.QtWidgets import QMainWindow, QApplication, QFileDialog

from ui_main import Ui_ProtT as ui_main


class TranslationWorker(QObject):
    finished = Signal()
    progress = Signal(str)

    def __init__(self, parent_self, parent=None):
        super().__init__(parent)
        self.parent_self = parent_self

    def run(self):
        self.progress.emit(f"Starting Translation")

        config = ConfigParser()
        config.read(r"working_conf.conf")
        api_keys = ConfigParser()
        api_keys.read(config["main"]["api_key_path"])
        if config["translation"]["service"] == "deepl":
            t = translation.DeepL(api_keys["default"]["deepl_key"])

        else:
            t = translation.LibreTranslate(config["translation"]["service_url"],
                                           api_keys["default"]["libretranslate_key"])

        src_pad = getattr(pad, config["source"]["kind"].capitalize())(config["source"]["url"],
                                                                      api_keys["default"]["etherpad_key"])
        dst_pad = getattr(pad, config["destination"]["kind"].capitalize())(config["destination"]["url"],
                                                                           api_keys["default"]["etherpad_key"])

        p = prott_main.Prott(src_pad,
                             dst_pad,
                             config["source"]["language"].lower(),
                             config["destination"]["language"].lower(),
                             t,
                             int(config["main"]["header_size"]))

        while self.parent_self.translate_state:
            p.pull_diff_translate_push()
            QThread.msleep(500)

        self.finished.emit()


class ProttGui(QMainWindow, ui_main):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.translate_state = True
        self.libretrans = None
        self.deepl = None
        self.setupUi(self)
        self.api_keys = ConfigParser()
        self.inputwidgets = [
            self.src_lang_combo,
            self.dst_lang_combo,
            self.translator_combo,
            self.src_pad_combo,
            self.dst_pad_combo,
            self.src_url_LE,
            self.dst_url_LE,
            self.api_path_LE,
            self.translator_url_LE,
            self.headersize_spin,
            self.refresh_lan_btn,
            self.api_find_btn,
            self.actionLoad_Preset,
            self.actionSave_Preset,
        ]
        self.service_dict = {"DeepL": "deepl", "Libre Translate": "libretranslate"}

        self.inverse_service_dict = {v: k for k, v in self.service_dict.items()}

        # Connect Actions
        self.actionDark_Mode.triggered.connect(self.toggle_stylesheet)
        self.actionSave_Preset.triggered.connect(self.save_preset_func)
        self.actionLoad_Preset.triggered.connect(self.load_preset_func)

        # Connect Buttons
        self.startstop_btn.clicked.connect(self.toggle_button)
        self.api_find_btn.clicked.connect(self.locate_api_key_func)
        self.src_open_btn.clicked.connect(self.open_src_browser)
        self.dst_open_btn.clicked.connect(self.open_dst_browser)
        self.refresh_lan_btn.clicked.connect(self.populate_lan_combos)
        self.translator_combo.currentIndexChanged.connect(self.deepl_disables_LE)
        self.deepl_disables_LE()

    @staticmethod
    def url_validator(x):
        result = urlparse(x)
        return all([result.scheme, result.netloc])

    def deepl_disables_LE(self):
        if self.translator_combo.currentText() == "DeepL":
            self.translator_url_LE.setDisabled(True)
        else:
            self.translator_url_LE.setDisabled(False)

    def disable_inputs(self, state: bool):
        for w in self.inputwidgets:
            w.setDisabled(state)
        if not state:
            self.deepl_disables_LE()

    def populate_lan_combos(self):
        src_lans = [""]
        dst_lans = [""]
        if os.path.exists(self.api_path_LE.text()) \
                and self.url_validator(self.translator_url_LE.text()) \
                or os.path.exists(self.api_path_LE.text()
                                  and self.translator_combo.currentText() == "DeepL"
                                  ):
            self.api_keys.read(self.api_path_LE.text())
            if self.translator_combo.currentText() == "DeepL":
                self.deepl = translation.DeepL(self.api_keys["default"]["deepl_key"])
                src_lans = self.deepl.get_supported_src_languages()
                dst_lans = self.deepl.get_supported_src_languages()

            elif self.translator_combo.currentText() == "Libre Translate":
                self.libretrans = translation.LibreTranslate(
                    self.translator_url_LE.text(),
                    self.api_keys["default"]["libretranslate_key"],
                )
                src_lans = self.libretrans.get_supported_src_languages()
                dst_lans = self.libretrans.get_supported_src_languages()

        self.src_lang_combo.clear()
        self.src_lang_combo.addItems([x.upper() for x in src_lans])

        self.dst_lang_combo.clear()
        self.dst_lang_combo.addItems([x.upper() for x in dst_lans])

    def toggle_stylesheet(self, e):
        if e:
            style_sheet_file = QFile(Path("style.qss"))
            if style_sheet_file.open(QFile.ReadOnly | QFile.Text):
                qss = QTextStream(style_sheet_file)
                style_sheet = qss.readAll()
                self.setStyleSheet(style_sheet)
                style_sheet_file.close()

        else:
            self.setStyleSheet("")

    def locate_api_key_func(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setNameFilter("Config Files (*.conf)")

        if dlg.exec():
            self.api_path_LE.setText(dlg.selectedFiles()[0])

    def open_src_browser(self):
        url = self.src_url_LE.text()
        if self.url_validator(url):
            webbrowser.open(url, new=0, autoraise=True)
        else:
            print("url not valid")

    def open_dst_browser(self):
        url = self.dst_url_LE.text()
        if self.url_validator(url):
            webbrowser.open(url, new=0, autoraise=True)
        else:
            print("url not valid")

    def create_conf_from_current(self, conf_name: str):
        working_conf = ConfigParser()
        working_conf["main"] = {
            "header_size": self.headersize_spin.text(),
            "api_key_path": self.api_path_LE.text(),
        }

        if self.translator_combo.currentText() == "DeepL":
            service_url = ""
        else:
            service_url = self.translator_url_LE.text()
        working_conf["translation"] = {
            "service": self.service_dict[self.translator_combo.currentText()],
            "service_url": service_url,
        }

        working_conf["source"] = {
            "kind": self.src_pad_combo.currentText().lower(),
            "url": self.src_url_LE.text(),
            "language": self.src_lang_combo.currentText(),
        }

        working_conf["destination"] = {
            "kind": self.dst_pad_combo.currentText().lower(),
            "url": self.dst_url_LE.text(),
            "language": self.dst_lang_combo.currentText(),
        }
        with open(conf_name, "w") as f:
            working_conf.write(f)

    def toggle_button(self, start):
        if start:
            self.disable_inputs(True)
            self.create_conf_from_current("working_conf.conf")
            try:
                utils.validate_config("working_conf.conf")
            except (OSError, FileNotFoundError, ValueError) as e:
                print(e)
                self.disable_inputs(False)
                self.startstop_btn.setChecked(False)
                return

            self.startstop_btn.setText("Stop")
            self.start_worker()

        else:
            self.translate_state = False
            self.disable_inputs(False)
            self.startstop_btn.setText("Start")

    def start_worker(self):
        # Worker
        # Create a QThread instance and a Worker instance
        self.thread = QThread()
        self.worker = TranslationWorker(self)

        # Move the worker to the thread
        self.worker.moveToThread(self.thread)

        # Connect signals and slots
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.progress.connect(lambda message: print(message))

        # Start the thread and the worker
        self.thread.started.connect(self.worker.run)

        self.translate_state = True
        self.thread.start()

    def save_preset_func(self):
        self.create_conf_from_current("temp_conf.conf")
        try:
            utils.validate_config("temp_conf.conf")
        except (OSError, FileNotFoundError, ValueError):
            print("invalid config")
            return

        save_dlg = QFileDialog()
        save_dlg.setFileMode(QFileDialog.AnyFile)
        save_dlg.setNameFilter("Config Files (*.conf)")

        if save_dlg.exec():
            filename = save_dlg.selectedFiles()[0]
            self.create_conf_from_current(filename)

        os.remove("temp_conf.conf")

    def load_preset_func(self):
        load_dlg = QFileDialog()
        load_dlg.setFileMode(QFileDialog.AnyFile)
        load_dlg.setNameFilter("Config Files (*.conf)")

        if load_dlg.exec():
            filename = load_dlg.selectedFiles()[0]
            try:
                prott.utils.validate_config(filename)
            except (OSError, FileNotFoundError, ValueError, configparser.NoSectionError):
                print("invalid Conf")
                return

            load_config = ConfigParser()
            load_config.read(filename)

            # Set or load language
            load_src_lan = load_config["source"]["language"].upper()
            load_dst_lan = load_config["destination"]["language"].upper()
            if load_src_lan in [
                self.src_lang_combo.itemText(i)
                for i in range(self.src_lang_combo.count())
            ]:
                self.src_lang_combo.setCurrentText(load_src_lan)
            else:
                self.src_lang_combo.clear()
                self.src_lang_combo.addItems([load_src_lan])
            if load_dst_lan in [
                self.dst_lang_combo.itemText(i)
                for i in range(self.dst_lang_combo.count())
            ]:
                self.dst_lang_combo.setCurrentText(load_dst_lan)
            else:
                self.dst_lang_combo.clear()
                self.dst_lang_combo.addItems([load_dst_lan])

            self.src_lang_combo.addItems([])
            self.dst_lang_combo.addItems([])
            self.translator_combo.setCurrentText(
                self.inverse_service_dict[load_config["translation"]["service"]]
            )
            self.src_pad_combo.setCurrentText(
                load_config["source"]["kind"].capitalize()
            )
            self.dst_pad_combo.setCurrentText(
                load_config["destination"]["kind"].capitalize()
            )
            self.src_url_LE.setText(load_config["source"]["url"])
            self.dst_url_LE.setText(load_config["destination"]["url"])
            self.translator_url_LE.setText(load_config["translation"]["service_url"])
            self.headersize_spin.setValue(int(load_config["main"]["header_size"]))
            self.api_path_LE.setText(load_config["main"]["api_key_path"])


if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = ProttGui()
    win.show()
    sys.exit(app.exec())
