# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'maindQVAdS.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (
    QCoreApplication,
    QDate,
    QDateTime,
    QLocale,
    QMetaObject,
    QObject,
    QPoint,
    QRect,
    QSize,
    QTime,
    QUrl,
    Qt,
)
from PySide6.QtGui import (
    QAction,
    QBrush,
    QColor,
    QConicalGradient,
    QCursor,
    QFont,
    QFontDatabase,
    QGradient,
    QIcon,
    QImage,
    QKeySequence,
    QLinearGradient,
    QPainter,
    QPalette,
    QPixmap,
    QRadialGradient,
    QTransform,
)
from PySide6.QtWidgets import (
    QApplication,
    QComboBox,
    QFrame,
    QGridLayout,
    QHBoxLayout,
    QLabel,
    QLayout,
    QLineEdit,
    QMainWindow,
    QMenu,
    QMenuBar,
    QPushButton,
    QSizePolicy,
    QSpacerItem,
    QSpinBox,
    QStatusBar,
    QVBoxLayout,
    QWidget,
)


class Ui_ProtT(object):
    def setupUi(self, ProtT):
        if not ProtT.objectName():
            ProtT.setObjectName("ProtT")
        ProtT.setEnabled(True)
        ProtT.resize(350, 491)
        ProtT.setMinimumSize(QSize(350, 0))
        self.actionDark_Mode = QAction(ProtT)
        self.actionDark_Mode.setObjectName("actionDark_Mode")
        self.actionDark_Mode.setCheckable(True)
        self.actionSave_Preset = QAction(ProtT)
        self.actionSave_Preset.setObjectName("actionSave_Preset")
        self.actionLoad_Preset = QAction(ProtT)
        self.actionLoad_Preset.setObjectName("actionLoad_Preset")
        self.centralwidget = QWidget(ProtT)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalSpacer = QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding
        )

        self.gridLayout.addItem(self.verticalSpacer, 6, 0, 1, 1)

        self.translator_url_lab = QLabel(self.centralwidget)
        self.translator_url_lab.setObjectName("translator_url_lab")
        self.translator_url_lab.setEnabled(True)

        self.gridLayout.addWidget(self.translator_url_lab, 2, 0, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding
        )

        self.gridLayout.addItem(self.verticalSpacer_2, 14, 0, 1, 1)

        self.frame_5 = QFrame(self.centralwidget)
        self.frame_5.setObjectName("frame_5")
        self.frame_5.setFrameShape(QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.frame_5)
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalLayout_3.setSizeConstraint(QLayout.SetNoConstraint)
        self.horizontalLayout_3.setContentsMargins(0, 5, 0, 5)
        self.api_path_LE = QLineEdit(self.frame_5)
        self.api_path_LE.setObjectName("api_path_LE")

        self.horizontalLayout_3.addWidget(self.api_path_LE)

        self.api_find_btn = QPushButton(self.frame_5)
        self.api_find_btn.setObjectName("api_find_btn")
        self.api_find_btn.setMaximumSize(QSize(130, 16777215))

        self.horizontalLayout_3.addWidget(self.api_find_btn)

        self.gridLayout.addWidget(self.frame_5, 5, 0, 1, 1)

        self.translator_type_lab = QLabel(self.centralwidget)
        self.translator_type_lab.setObjectName("translator_type_lab")

        self.gridLayout.addWidget(self.translator_type_lab, 0, 0, 1, 1)

        self.frame_3 = QFrame(self.centralwidget)
        self.frame_3.setObjectName("frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_3)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_2 = QFrame(self.frame_3)
        self.frame_2.setObjectName("frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout.setContentsMargins(-1, -1, -1, 0)
        self.src_pad_combo = QComboBox(self.frame_2)
        self.src_pad_combo.addItem("")
        self.src_pad_combo.addItem("")
        self.src_pad_combo.setObjectName("src_pad_combo")

        self.verticalLayout.addWidget(self.src_pad_combo)

        self.src_url_lab = QLabel(self.frame_2)
        self.src_url_lab.setObjectName("src_url_lab")

        self.verticalLayout.addWidget(self.src_url_lab)

        self.src_url_LE = QLineEdit(self.frame_2)
        self.src_url_LE.setObjectName("src_url_LE")

        self.verticalLayout.addWidget(self.src_url_LE)

        self.src_open_btn = QPushButton(self.frame_2)
        self.src_open_btn.setObjectName("src_open_btn")

        self.verticalLayout.addWidget(self.src_open_btn)

        self.src_lang_lab = QLabel(self.frame_2)
        self.src_lang_lab.setObjectName("src_lang_lab")

        self.verticalLayout.addWidget(self.src_lang_lab)

        self.src_lang_combo = QComboBox(self.frame_2)
        self.src_lang_combo.setObjectName("src_lang_combo")

        self.verticalLayout.addWidget(self.src_lang_combo)

        self.horizontalLayout_2.addWidget(self.frame_2)

        self.frame_4 = QFrame(self.frame_3)
        self.frame_4.setObjectName("frame_4")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_4)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, -1, -1, 0)
        self.dst_pad_combo = QComboBox(self.frame_4)
        self.dst_pad_combo.addItem("")
        self.dst_pad_combo.addItem("")
        self.dst_pad_combo.setObjectName("dst_pad_combo")

        self.verticalLayout_2.addWidget(self.dst_pad_combo)

        self.dst_url_lab = QLabel(self.frame_4)
        self.dst_url_lab.setObjectName("dst_url_lab")

        self.verticalLayout_2.addWidget(self.dst_url_lab)

        self.dst_url_LE = QLineEdit(self.frame_4)
        self.dst_url_LE.setObjectName("dst_url_LE")

        self.verticalLayout_2.addWidget(self.dst_url_LE)

        self.dst_open_btn = QPushButton(self.frame_4)
        self.dst_open_btn.setObjectName("dst_open_btn")

        self.verticalLayout_2.addWidget(self.dst_open_btn)

        self.dst_lang_lab = QLabel(self.frame_4)
        self.dst_lang_lab.setObjectName("dst_lang_lab")

        self.verticalLayout_2.addWidget(self.dst_lang_lab)

        self.dst_lang_combo = QComboBox(self.frame_4)
        self.dst_lang_combo.setObjectName("dst_lang_combo")

        self.verticalLayout_2.addWidget(self.dst_lang_combo)

        self.horizontalLayout_2.addWidget(self.frame_4)

        self.gridLayout.addWidget(self.frame_3, 8, 0, 1, 1)

        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName("frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_4 = QSpacerItem(
            40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum
        )

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.frame_6 = QFrame(self.frame)
        self.frame_6.setObjectName("frame_6")
        self.frame_6.setFrameShape(QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.frame_6)
        self.horizontalLayout_4.setSpacing(9)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.headersize_lb = QLabel(self.frame_6)
        self.headersize_lb.setObjectName("headersize_lb")

        self.horizontalLayout_4.addWidget(self.headersize_lb)

        self.headersize_spin = QSpinBox(self.frame_6)
        self.headersize_spin.setObjectName("headersize_spin")

        self.horizontalLayout_4.addWidget(self.headersize_spin)

        self.horizontalLayout.addWidget(self.frame_6)

        self.horizontalSpacer_3 = QSpacerItem(
            40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum
        )

        self.horizontalLayout.addItem(self.horizontalSpacer_3)

        self.startstop_btn = QPushButton(self.frame)
        self.startstop_btn.setObjectName("startstop_btn")
        self.startstop_btn.setEnabled(True)
        self.startstop_btn.setMinimumSize(QSize(100, 40))
        self.startstop_btn.setCheckable(True)

        self.horizontalLayout.addWidget(self.startstop_btn)

        self.horizontalSpacer = QSpacerItem(
            40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum
        )

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.gridLayout.addWidget(self.frame, 13, 0, 1, 1)

        self.api_lab = QLabel(self.centralwidget)
        self.api_lab.setObjectName("api_lab")

        self.gridLayout.addWidget(self.api_lab, 4, 0, 1, 1)

        self.translator_combo = QComboBox(self.centralwidget)
        self.translator_combo.addItem("")
        self.translator_combo.addItem("")
        self.translator_combo.setObjectName("translator_combo")

        self.gridLayout.addWidget(self.translator_combo, 1, 0, 1, 1)

        self.translator_url_LE = QLineEdit(self.centralwidget)
        self.translator_url_LE.setObjectName("translator_url_LE")

        self.gridLayout.addWidget(self.translator_url_LE, 3, 0, 1, 1)

        self.frame_7 = QFrame(self.centralwidget)
        self.frame_7.setObjectName("frame_7")
        self.frame_7.setFrameShape(QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_7)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(-1, 0, -1, -1)
        self.refresh_lan_btn = QPushButton(self.frame_7)
        self.refresh_lan_btn.setObjectName("refresh_lan_btn")

        self.horizontalLayout_5.addWidget(self.refresh_lan_btn)

        self.horizontalSpacer_5 = QSpacerItem(
            40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum
        )

        self.horizontalLayout_5.addItem(self.horizontalSpacer_5)

        self.gridLayout.addWidget(self.frame_7, 9, 0, 1, 1)

        ProtT.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(ProtT)
        self.statusbar.setObjectName("statusbar")
        ProtT.setStatusBar(self.statusbar)
        self.menuBar = QMenuBar(ProtT)
        self.menuBar.setObjectName("menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 350, 22))
        self.menuView = QMenu(self.menuBar)
        self.menuView.setObjectName("menuView")
        self.menuFile = QMenu(self.menuBar)
        self.menuFile.setObjectName("menuFile")
        ProtT.setMenuBar(self.menuBar)

        self.menuBar.addAction(self.menuFile.menuAction())
        self.menuBar.addAction(self.menuView.menuAction())
        self.menuView.addAction(self.actionDark_Mode)
        self.menuFile.addAction(self.actionSave_Preset)
        self.menuFile.addAction(self.actionLoad_Preset)

        self.retranslateUi(ProtT)

        QMetaObject.connectSlotsByName(ProtT)

    # setupUi

    def retranslateUi(self, ProtT):
        ProtT.setWindowTitle(QCoreApplication.translate("ProtT", "ProTT", None))
        self.actionDark_Mode.setText(
            QCoreApplication.translate("ProtT", "Dark Mode", None)
        )
        # if QT_CONFIG(tooltip)
        self.actionDark_Mode.setToolTip(
            QCoreApplication.translate("ProtT", "Enable or Disable Dark Mode", None)
        )
        # endif // QT_CONFIG(tooltip)
        self.actionSave_Preset.setText(
            QCoreApplication.translate("ProtT", "Save Preset", None)
        )
        self.actionLoad_Preset.setText(
            QCoreApplication.translate("ProtT", "Load Preset", None)
        )
        self.translator_url_lab.setText(
            QCoreApplication.translate("ProtT", "Translator URL", None)
        )
        self.api_find_btn.setText(
            QCoreApplication.translate("ProtT", "Locate API Key", None)
        )
        self.translator_type_lab.setText(
            QCoreApplication.translate("ProtT", "Translator Type", None)
        )
        self.src_pad_combo.setItemText(
            0, QCoreApplication.translate("ProtT", "Etherpad", None)
        )
        self.src_pad_combo.setItemText(
            1, QCoreApplication.translate("ProtT", "Hedgedoc", None)
        )

        self.src_url_lab.setText(
            QCoreApplication.translate("ProtT", "Source Pad URL", None)
        )
        self.src_open_btn.setText(
            QCoreApplication.translate("ProtT", "Open in Browser", None)
        )
        self.src_lang_lab.setText(
            QCoreApplication.translate("ProtT", "Source Language", None)
        )
        self.dst_pad_combo.setItemText(
            0, QCoreApplication.translate("ProtT", "Etherpad", None)
        )
        self.dst_pad_combo.setItemText(
            1, QCoreApplication.translate("ProtT", "Hedgedoc", None)
        )

        self.dst_url_lab.setText(
            QCoreApplication.translate("ProtT", "Destination Pad URL", None)
        )
        self.dst_open_btn.setText(
            QCoreApplication.translate("ProtT", "Open in Browser", None)
        )
        self.dst_lang_lab.setText(
            QCoreApplication.translate("ProtT", "Destination Language", None)
        )
        self.headersize_lb.setText(
            QCoreApplication.translate("ProtT", "Header Size", None)
        )
        self.startstop_btn.setText(QCoreApplication.translate("ProtT", "Start", None))
        self.api_lab.setText(
            QCoreApplication.translate("ProtT", "Path to API Keys File", None)
        )
        self.translator_combo.setItemText(
            0, QCoreApplication.translate("ProtT", "DeepL", None)
        )
        self.translator_combo.setItemText(
            1, QCoreApplication.translate("ProtT", "Libre Translate", None)
        )

        self.refresh_lan_btn.setText(
            QCoreApplication.translate("ProtT", "Refresh Languages", None)
        )
        self.menuView.setTitle(QCoreApplication.translate("ProtT", "View", None))
        self.menuFile.setTitle(QCoreApplication.translate("ProtT", "File", None))

    # retranslateUi
